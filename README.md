ScrollyLcd
==========
This code is a very basic way of enabling the traditional methods such as println() and friends to work correctly when using the Arduino LiquidCrystal library module.

It works by intercepting certain methods from the LiquidCrystal class (which it inherits from), which allows it to keep track of the current state of the display. Then,
when the traditional \r\n sequence is spotted (note that currently it relies exclusively on the \n - it detects and ignores \r), it moves the cursor position to the next line.

The class also keeps track of the contents of the screen, and when the text scrolls off the end of the display, it removes the first line, facilitating never ending vertical scrolling.

![Image](demo.gif "demo")

Usage
-----
The usage of this library is very straightforward.

Copy the two files,  ScrollyLcd.cpp  ScrollyLcd.h into your Arduino project folder. Instead of 

`#include <LiquidCrystal.h>`

replace with:

`#include "ScrollyLcd.h`

ScrollyLcd is a drop-in replacement for LiquidCrystal, meaning it can be used without changes to your code. Note that calls to begin() clear the screen, as ScrollyLcd's internal tracking of the
display must be kept in sync with the actual display.