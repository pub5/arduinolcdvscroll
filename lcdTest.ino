#include "ScrollyLcd.h"

ScrollyLcd   lcd(2,3,4,5,6,7);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(38400);
  Serial.println("Starting");
  
  lcd.begin(20, 4);
  lcd.clear();
  lcd.print("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
}

long theTime = 0;
void loop() {
  // put your main code here, to run repeatedly:

  if (millis() - theTime > 1000) {
    theTime = millis();
    lcd.println();
    lcd.print(millis());
  }
}
