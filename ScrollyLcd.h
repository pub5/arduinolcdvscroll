#ifndef _SCROLLYLCD_H
#define _SCROLLYLCD_H
#include <LiquidCrystal.h>


class ScrollyLcd: public LiquidCrystal {
  public:
    ScrollyLcd(uint8_t rs, uint8_t enable, uint8_t d4, uint8_t d5, uint8_t d6, uint8_t d7);

    ScrollyLcd(uint8_t rs, uint8_t rw, uint8_t enable, uint8_t d4, uint8_t d5, uint8_t d6, uint8_t d7);
    ScrollyLcd(uint8_t rs, uint8_t enable, uint8_t d0, uint8_t d1, uint8_t d2, uint8_t d3, uint8_t d4, uint8_t d5, uint8_t d6, uint8_t d7);
    ScrollyLcd(uint8_t rs, uint8_t rw, uint8_t enable, uint8_t d0, uint8_t d1, uint8_t d2, uint8_t d3, uint8_t d4, uint8_t d5, uint8_t d6, uint8_t d7);
    
    size_t write(uint8_t theChar);
    void begin(uint8_t width, uint8_t height);
    void clear();
  private:
    void reposition();
    void vScroll(); // remove the first line, add a line to the end, and reprint the entire screen
    int mWidth, mHeight;    
    int mCursorRow, mCursorColumn;
    String *mBuffer;
};

#endif// _SCROLLYLCD_H
