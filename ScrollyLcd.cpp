#include <LiquidCrystal.h>
#include "ScrollyLcd.h"

ScrollyLcd::ScrollyLcd(uint8_t rs, uint8_t enable, uint8_t d4, uint8_t d5, uint8_t d6, uint8_t d7)
  :LiquidCrystal(rs, enable, d4, d5, d6, d7) {
}

ScrollyLcd::ScrollyLcd(uint8_t rs, uint8_t rw, uint8_t enable, uint8_t d4, uint8_t d5, uint8_t d6, uint8_t d7)
  :LiquidCrystal(rs, rw, enable, d4, d5, d6, d7){
}
ScrollyLcd::ScrollyLcd(uint8_t rs, uint8_t enable, uint8_t d0, uint8_t d1, uint8_t d2, uint8_t d3, uint8_t d4, uint8_t d5, uint8_t d6, uint8_t d7)
  :LiquidCrystal(rs, enable, d0, d1, d2, d3, d4, d5, d6, d7){
}
ScrollyLcd::ScrollyLcd(uint8_t rs, uint8_t rw, uint8_t enable, uint8_t d0, uint8_t d1, uint8_t d2, uint8_t d3, uint8_t d4, uint8_t d5, uint8_t d6, uint8_t d7)
  :LiquidCrystal(rs, rw, enable, d0, d1, d2, d3, d4, d5, d6, d7){
}

void ScrollyLcd::clear() {
  mCursorRow = 0;
  mCursorColumn = 0;
  LiquidCrystal::clear();
}

void ScrollyLcd::begin(uint8_t w, uint8_t h) {
  LiquidCrystal::begin(w, h);
  mWidth = w;
  mHeight = h;
  mCursorRow = 0;
  mCursorColumn = 0;
  LiquidCrystal::clear();
  // we keep track of the cursor position manually because we can't read from the screen. Thus we need to start in a known state (i.e. clear).

  mBuffer = new String[mHeight];
}

void ScrollyLcd::reposition() {
  setCursor(mCursorColumn, mCursorRow);
}

void ScrollyLcd::vScroll() {
  // swap out row 0, shuffle up and then plonk back down, clear the now last line, redraw 

  for (int i=1; i < mHeight; i++) 
    mBuffer[i-1] = mBuffer[i];

  LiquidCrystal::clear();

  for (int i = 0; i < mHeight-1; i++) {
    setCursor(0, i);
    for (char *pt = mBuffer[i].c_str(); *pt; pt++)
      LiquidCrystal::write(*pt);
  }
  mBuffer[mHeight - 1] = "";
  mCursorColumn = 0;
  mCursorRow = mHeight - 1;
  reposition();
  return;
}

size_t ScrollyLcd::write(uint8_t theChar) {
  switch (theChar) {
    case '\r':
        // Yes this is Linux like. However this class is simple, and doesn't play nice with overwriting in the middle of the line
        break;
    case '\n':
        mCursorColumn = 0;
        mCursorRow++;
        if (mCursorRow >= mHeight)
           vScroll();
        else
          reposition();
        break;
    default:
        if (mCursorColumn < mWidth) {
          mBuffer[mCursorRow].concat((char)theChar);
          mCursorColumn += LiquidCrystal::write(theChar);
        } else {
          write('\n');
          write(theChar);
        }
  }
  return 1;
}
